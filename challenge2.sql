drop table highrange;
drop table maxhigh;
drop table maxtime;

Create table highrange as 
select distinct date_format(Date,'%m/%d/%Y') AS trade_date,
format(abs(Open-Close),4) as price_range
from ibm.stocks
order by price_range desc
limit 3;

Create table maxhigh as 
select date_format(Date,'%m/%d/%Y') as max_date,
max(high) as max_price
from ibm.stocks
where date_format(Date,'%m/%d/%Y') in (select trade_date from highrange h)
group by date
order by high desc;

Create table maxtime as 
select date_format(Date,'%m/%d/%Y') as max_date_2, Time
from ibm.stocks join maxhigh mh on date_format(Date,'%m/%d/%Y')=mh.max_date
join highrange h on h.trade_date=date_format(Date,'%m/%d/%Y')
where stocks.high = mh.max_price;

select trade_date, price_range,time 
from highrange h join maxtime m on h.trade_date=m.max_date_2
