set @start_time = '2010-10-11 09:00:00', @end_time = '2010-10-11 14:00:00';

SELECT sum(close*vol)/sum(vol) as VWAP,
date_format(tdate,'%d/%m/%Y') AS trade_date,
time_format(@start_time,'%H:%i') AS start_time,
time_format(@end_time,'%H:%i') AS end_time
from stocks
where tdate between @start_time and @end_time
group by trade_date

